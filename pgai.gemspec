# frozen_string_literal: true

require_relative "lib/pgai/version"

Gem::Specification.new do |spec|
  spec.name = "pgai"
  spec.version = Pgai::VERSION
  spec.authors = ["Marius Bobin"]
  spec.email = ["mbobin@gitlab.com"]
  spec.license = "MIT"

  spec.summary = "CLI wrapper for postgres.ai thin clones"
  spec.homepage = "https://gitlab.com/mbobin/pgai"
  spec.required_ruby_version = ">= 3.0.0"

  spec.files = Dir["lib/**/*", "MIT-LICENSE", "README.md"]
  spec.executables = %w[pgai]

  spec.add_dependency "thor", "~> 1.2", ">= 1.2.1"
  spec.add_dependency "zeitwerk", "~> 2.6", ">= 2.6.13"
  spec.add_dependency "excon", "~> 0.109.0"
  spec.add_dependency "net-ssh", "~> 7.2", ">= 7.2.1"
  spec.add_dependency "ed25519", ">= 1.2", "< 2.0"
  spec.add_dependency "bcrypt_pbkdf", ">= 1.0", "< 2.0"
  spec.add_dependency "tty-progressbar", "~> 0.18.2"
  spec.add_dependency "tty-logger", "~> 0.6.0"
  spec.add_dependency "concurrent-ruby", "~> 1.2", ">= 1.2.3"

  spec.add_development_dependency "rake", "~> 13.0"
  spec.add_development_dependency "rspec", "~> 3.0"
  spec.add_development_dependency "standard", "~> 1.3"
  spec.add_development_dependency "simplecov", "~> 0.22.0"
end

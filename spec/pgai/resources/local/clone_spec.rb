# frozen_string_literal: true

RSpec.describe Pgai::Resources::Local::Clone do
  it { is_expected.to have_attribute(:id) }
  it { is_expected.to have_attribute(:host) }
  it { is_expected.to have_attribute(:remote_host) }
  it { is_expected.to have_attribute(:port) }
  it { is_expected.to have_attribute(:password) }
  it { is_expected.to have_attribute(:user) }
  it { is_expected.to have_attribute(:dbname) }
  it { is_expected.to have_attribute(:created_at) }
  it { is_expected.to have_attribute(:data_state_at) }

  context "connection strings" do
    let(:attibutes) do
      {
        host: "localhost",
        port: 5432,
        remote_host: "127.0.0.1",
        user: "postgres",
        dbname: "dblab",
        password: "secret"
      }
    end

    let(:record) { described_class.new(attibutes) }

    before do
      expect(Pgai::Port::Allocator).to receive(:pick).and_return(6859)
    end

    describe "#connection_string" do
      it "generates a connection string" do
        expect(record.connection_string)
          .to eq("'host=localhost port=6859 user=postgres dbname=dblab password=secret'")
      end
    end

    describe "#database_url" do
      it "generates a connection string" do
        expect(record.database_url)
          .to eq("postgresql://postgres:secret@localhost:6859/dblab")
      end
    end

    describe "#with_port_forward" do
      let(:forward_manager) { Pgai::Port::Manager.new }

      it "starts a port forward and yields control" do
        expect(forward_manager).to receive(:forward).with(6859, "127.0.0.1", 5432).and_call_original
        expect(forward_manager).to receive(:start).with(6859, "127.0.0.1", 5432)

        expect { |b| record.with_port_forward(forward_manager, &b) }.to yield_with_args(record)
      end
    end
  end
end

# frozen_string_literal: true

RSpec.describe Pgai::Resources::Local::Environment do
  it { is_expected.to have_attribute(:id) }
  it { is_expected.to have_attribute(:alias) }
  it { is_expected.to have_attribute(:port) }
  it { is_expected.to have_attribute(:dbname) }
  it { is_expected.to have_attribute(:access_token) }
  it { is_expected.to have_attribute(:clone_prefix) }

  before do
    allow(Pgai::Port::Allocator).to receive(:pick).and_return(6858)
  end

  describe "#client" do
    let(:record) { described_class.new(access_token: "secret") }

    it { expect(record.client).to be_a(Pgai::Client) }
  end

  describe "#with_port_forward" do
    let(:forward_manager) { Pgai::Port::Manager.new }
    let(:record) { described_class.new(id: "ci.lab.internal", port: 5432) }

    it "starts a port forward and yields control" do
      expect(forward_manager).to receive(:forward).with(6858, "ci.lab.internal", 5432).and_call_original
      expect(forward_manager).to receive(:start).with(6858, "ci.lab.internal", 5432)

      expect { |b| record.with_port_forward(forward_manager, &b) }.to yield_with_args(record)
    end
  end

  describe "#expected_cloning_time" do
    it "delegates to client" do
      expect(subject.client).to receive(:expected_cloning_time).and_return(10)

      expect(subject.expected_cloning_time).to eq(10)
    end
  end

  describe "#clone_id" do
    let(:record) { described_class.new(clone_prefix: "test", alias: "main") }

    it { expect(record.clone_id).to eq("test_main") }
  end
end

# frozen_string_literal: true

RSpec.describe Pgai::Resources::Local::Configuration do
  it { is_expected.to have_attribute(:id) }
  it { is_expected.to have_attribute(:access_token) }
  it { is_expected.to have_attribute(:clone_prefix) }

  it { expect(subject.id).to eq("config") }

  describe ".default" do
    before do
      described_class.new(access_token: "secret", clone_prefix: "test").save
    end

    subject(:config) { described_class.default }

    it { expect(config.id).to eq("config") }
    it { expect(config.access_token).to eq("secret") }
    it { expect(config.clone_prefix).to eq("test") }
  end
end

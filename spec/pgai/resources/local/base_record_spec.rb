# frozen_string_literal: true

RSpec.describe Pgai::Resources::Local::BaseRecord do
  let(:test_class) do
    Class.new(Pgai::Resources::Local::BaseRecord) do
      attribute :id, :integer
      attribute :username, :string

      def self.name
        "TestClass"
      end
    end
  end

  let(:attributes) { {id: 43, username: "bacon"} }
  let(:record) { test_class.new(attributes) }

  before do
    record.save
  end

  describe ".all" do
    it { expect(test_class.all).to be_all(test_class) }
    it { expect(test_class.all.map(&:attributes)).to match([attributes]) }
  end

  describe ".find" do
    it { expect(test_class.find(43)).to be_a(test_class) }
    it { expect(test_class.find(43).attributes).to match(attributes) }
  end

  describe ".delete" do
    it { expect { test_class.delete(43) }.to change { test_class.find(43) }.to(nil) }
  end

  describe ".record_type" do
    it { expect(test_class.record_type).to eq(:testclasss) }
  end

  describe ".backend" do
    it { expect(test_class.backend).to be_a(Pgai::Store) }
  end

  describe "#save" do
    it "updates attributes", :aggregate_failures do
      record.username = "bacon_2"

      expect(record.save).to be_truthy
      expect(record.username).to eq("bacon_2")
      expect(test_class.find(record.id).username).to eq("bacon_2")
    end
  end

  describe "#delete" do
    it { expect { record.delete }.to change { test_class.find(43) }.to(nil) }
  end
end

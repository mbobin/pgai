# frozen_string_literal: true

RSpec.describe Pgai::Resources::Remote::Clone do
  it { is_expected.to have_attribute(:id) }
  it { is_expected.to have_attribute(:protected) }
  it { is_expected.to have_attribute(:created_at) }
  it { is_expected.to have_attribute(:delete_at) }
end

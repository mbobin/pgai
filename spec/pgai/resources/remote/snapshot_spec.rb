# frozen_string_literal: true

RSpec.describe Pgai::Resources::Remote::Snapshot do
  it { is_expected.to have_attribute(:id) }
  it { is_expected.to have_attribute(:created_at) }
  it { is_expected.to have_attribute(:data_state_at) }
  it { is_expected.to have_attribute(:physical_size) }
  it { is_expected.to have_attribute(:logical_size) }
  it { is_expected.to have_attribute(:pool) }
  it { is_expected.to have_attribute(:num_clones) }

  describe ".path" do
    it { expect(described_class.path).to eq("snapshots") }
  end

  describe ".all" do
    let(:token) { "demo-token" }
    let(:host) { "https://127.0.0.1:2355" }
    let(:response) do
      [
        {
          id: "dblab_pool_01/clone_pre_20240326124500@snapshot_20240326124504",
          createdAt: "2024-03-26T12:51:40Z",
          dataStateAt: "2024-03-26T12:45:04Z",
          physicalSize: 90262409216,
          logicalSize: 25338678614016,
          pool: "dblab_pool_01",
          numClones: 3
        }
      ]
    end

    around do |example|
      described_class.with_client(Pgai::Client.new(token: token, host: host)) do
        example.run
      end
    end

    before do
      Excon.stub({
        scheme: "https",
        host: URI(host).hostname,
        path: "/snapshots",
        port: URI(host).port,
        headers: {
          "Verification-Token" => token
        }
      },
        {
          body: JSON.dump(response),
          status: 200
        })
    end

    subject(:resources) { described_class.all }
    let(:resource) { resources.first }

    it { expect(resources).to be_all(described_class) }
    it { expect(resources.size).to eq(1) }
    it { expect(resource.id).to eq("dblab_pool_01/clone_pre_20240326124500@snapshot_20240326124504") }
    it { expect(resource.created_at).to eq(::Time.at(::Time.new("2024-03-26T12:51:40Z", in: "UTC"))) }
    it { expect(resource.data_state_at).to eq(::Time.at(::Time.new("2024-03-26T12:45:04Z", in: "UTC"))) }
    it { expect(resource.physical_size).to eq(90262409216) }
    it { expect(resource.logical_size).to eq(25338678614016) }
    it { expect(resource.pool).to eq("dblab_pool_01") }
    it { expect(resource.num_clones).to eq(3) }
  end

  describe ".latest" do
    let(:token) { "demo-token" }
    let(:host) { "https://127.0.0.1:2355" }
    let(:response) do
      [
        {
          id: "dblab_pool_01/clone_pre_20240326124500@snapshot_20240326124504",
          createdAt: "2024-03-26T12:51:40Z",
          dataStateAt: "2024-03-26T12:45:04Z",
          physicalSize: 90262409216,
          logicalSize: 25338678614016,
          pool: "dblab_pool_01",
          numClones: 0
        },
        {
          id: "dblab_pool_01/clone_pre_20240326084500@snapshot_20240326084517",
          createdAt: "2024-03-26T08:50:51Z",
          dataStateAt: "2024-03-26T08:45:17Z",
          physicalSize: 102431371264,
          logicalSize: 25325452664320,
          pool: "dblab_pool_01",
          numClones: 1
        }

      ]
    end

    around do |example|
      described_class.with_client(Pgai::Client.new(token: token, host: host)) do
        example.run
      end
    end

    before do
      Excon.stub({
        scheme: "https",
        host: URI(host).hostname,
        path: "/snapshots",
        port: URI(host).port,
        headers: {
          "Verification-Token" => token
        }
      },
        {
          body: JSON.dump(response),
          status: 200
        })
    end

    subject(:resource) { described_class.latest }
    it { expect(resource.id).to eq("dblab_pool_01/clone_pre_20240326124500@snapshot_20240326124504") }
  end
end

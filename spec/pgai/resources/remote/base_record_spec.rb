# frozen_string_literal: true

RSpec.describe Pgai::Resources::Remote::BaseRecord do
  it { expect(described_class.client).to be_nil }
  it { expect(subject.client).to be_nil }

  let(:mock_client) { instance_double(Pgai::Client) }

  describe ".client" do
    it "can change client" do
      described_class.client = mock_client

      expect(described_class.client).to eq(mock_client)
      expect(described_class.new.client).to eq(mock_client)
    end
  end

  describe ".with_client" do
    let(:new_client) { instance_double(Pgai::Client) }

    it "changes client for the block execution" do
      described_class.client = mock_client

      described_class.with_client(new_client) do
        expect(described_class.client).to eq(new_client)
        expect(subject.client).to eq(new_client)
      end

      expect(described_class.client).to eq(mock_client)
      expect(subject.client).to eq(mock_client)
    end
  end

  describe ".refresh_attributes" do
    it "delegates to attributes" do
      expect(subject).to receive(:assign_attributes).with({a: :b, c: :d})

      subject.refresh_attributes(a: :b, c: :d)
    end
  end
end

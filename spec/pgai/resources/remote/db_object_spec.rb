# frozen_string_literal: true

RSpec.describe Pgai::Resources::Remote::DbObject do
  it { is_expected.to have_attribute(:username) }
  it { is_expected.to have_attribute(:password) }
  it { is_expected.to have_attribute(:restricted) }
  it { is_expected.to have_attribute(:host) }
  it { is_expected.to have_attribute(:port) }
  it { is_expected.to have_attribute(:db_name) }
  it { is_expected.to have_attribute(:conn_str) }

  it { expect(subject.username).to be_a(String) }
  it { expect(subject.password).to be_a(String) }
  it { expect(subject.restricted).to be_falsey }

  describe ".refresh_attributes" do
    it "delegates to attributes and skips some fields" do
      expect(subject)
        .to receive(:assign_attributes)
        .with({conn_str: "conn", db_name: "dblab", host: "localhost", port: 3321, restricted: true})

      subject.refresh_attributes(
        username: "new name",
        password: "secret",
        restricted: true,
        host: "localhost",
        port: 3321,
        db_name: "dblab",
        conn_str: "conn"
      )
    end
  end
end

# frozen_string_literal: true

RSpec.describe Pgai::Resources::Attributes do
  let(:test_class) do
    Class.new do
      include Pgai::Resources::Attributes

      attribute :username, :string, default: -> { "Bob" }
      attribute :name, :string
      attribute :age, :integer, default: 42
      attribute :active, :boolean
      attribute :last_seen_at, :datetime
      attribute :something, :decimal
    end
  end

  let(:attributes) { {} }
  subject(:record) { test_class.new(**attributes) }

  it { is_expected.to have_attribute(:username) }
  it { is_expected.to have_attribute(:name) }
  it { is_expected.to have_attribute(:age) }
  it { is_expected.to have_attribute(:active) }
  it { is_expected.to have_attribute(:last_seen_at) }
  it { is_expected.to have_attribute(:something) }

  context "with default values" do
    it { is_expected.to have_attributes(username: a_string_starting_with("B"), age: 42) }
  end

  context "with type casting" do
    let(:attributes) do
      {
        username: :bacon,
        name: 123,
        age: "43",
        active: "false",
        last_seen_at: "2024-03-21T08:35:09Z",
        something: "1.42"
      }
    end

    let(:expected_attributes) do
      {
        username: "bacon",
        name: "123",
        age: 43,
        active: false,
        last_seen_at: ::Time.at(::Time.new(attributes[:last_seen_at], in: "UTC")),
        something: 1.42
      }
    end

    it { is_expected.to have_attributes(expected_attributes) }
  end

  context "with class inheritance" do
    let(:child_class) do
      Class.new(test_class) do
        attribute :first_name, :string
      end
    end

    subject(:record) { child_class.new(**attributes) }

    it { is_expected.to have_attribute(:username) }
    it { is_expected.to have_attribute(:name) }
    it { is_expected.to have_attribute(:age) }
    it { is_expected.to have_attribute(:active) }
    it { is_expected.to have_attribute(:last_seen_at) }
    it { is_expected.to have_attribute(:first_name) }
  end
end

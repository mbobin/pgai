# frozen_string_literal: true

RSpec.describe Pgai::Port::Allocator do
  describe ".pick" do
    it { expect(described_class.pick).to be >= 5000 }
    it { expect(described_class.pick).to be <= 9000 }
  end

  describe "#pick" do
    let(:allocator) { described_class.new(start: 5678, finish: 5679) }

    it { expect(allocator.pick).to eq(5678) }

    context "when the first option is not available" do
      around do |example|
        server = TCPServer.new(described_class::LOCALHOST, 5678)
        example.run
        server.close
      end

      it { expect(allocator.pick).to eq(5679) }
    end
  end
end

# frozen_string_literal: true

RSpec.describe Pgai::Cli::Main do
  it "configures the application" do
    expect(Thor::LineEditor)
      .to receive(:readline)
      .with("Access token: ", {echo: false})
      .and_return("secret")

    described_class.start(["config", "--prefix", "test"])

    expect(Pgai::Resources::Local::Configuration.find("config"))
      .to have_attributes(clone_prefix: "test", access_token: "secret")
  end
end

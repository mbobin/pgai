# frozen_string_literal: true

RSpec.describe Pgai::Cli::Env do
  it "configures the environment" do
    described_class.start(["add", "-a", "ci", "-i", "ci.lab.internal", "-p", "1234", "-n", "dblab"])

    expect(Pgai::Resources::Local::Environment.find("ci"))
      .to have_attributes(alias: "ci", id: "ci.lab.internal", port: 1234, dbname: "dblab")
  end

  it "removes the environment" do
    Pgai::Resources::Local::Environment
      .new(alias: "ci", id: "lab-ci.db-lab.internal", port: 1234, dbname: "dblab")
      .save

    expect { described_class.start(["remove", "-a", "ci"]) }
      .to change { Pgai::Resources::Local::Environment.find("ci") }
      .to(nil)
  end

  it "lists the environments" do
    env = Pgai::Resources::Local::Environment.new(alias: "ci", id: "ci.lab.internal", port: 1234, dbname: "dblab")
    env.save
    data = JSON.pretty_generate([env.attributes.slice(:id, :alias, :port, :dbname, :snapshot)])

    expect($stdout).to receive(:print).with("#{data}\n")
    described_class.start(["list"])
  end
end

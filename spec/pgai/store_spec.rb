# frozen_string_literal: true

RSpec.describe Pgai::Store do
  subject(:store) { described_class.new }
  let(:record_type) { :events }

  let(:attributes) do
    {
      id: :id,
      value: 123
    }
  end

  let(:other_attributes) do
    {
      id: :other_id,
      value: "some value"
    }
  end

  let(:unrelated_attributes) do
    {
      id: :something,
      value: "will not be returned"
    }
  end

  describe "#save" do
    it "saves new records" do
      expect(store.save(record_type, attributes)).to be_truthy
      expect(store.find(record_type, :id)).to match(attributes)
    end

    it "updates existing records" do
      expect(store.save(record_type, attributes)).to be_truthy

      new_attributes = attributes.merge(value: 124)

      expect(store.save(record_type, new_attributes)).to be_truthy

      expect(store.find(record_type, :id)).to match(new_attributes)
    end
  end

  describe "#all" do
    before do
      store.save(record_type, attributes)
      store.save(record_type, other_attributes)
      store.save(:news, unrelated_attributes)
    end

    it "returns all event type records" do
      expect(store.all(record_type)).to match_array([attributes, other_attributes])
    end
  end

  describe "#delete" do
    before do
      store.save(record_type, attributes)
    end

    it "deletes the specified record" do
      expect { store.delete(record_type, :id) }.to change { store.find(record_type, :id) }
    end
  end

  describe "#find" do
    before do
      store.save(record_type, attributes)
    end

    it { expect(store.find(record_type, :id)).to match(attributes) }
    it { expect(store.find(record_type, :missing)).to be_nil }
    it { expect(store.find(:unknown, :missing)).to be_nil }
  end
end

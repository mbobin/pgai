# frozen_string_literal: true

RSpec.describe Pgai::Client do
  subject(:client) { described_class.new(token: token, host: host) }

  let(:token) { "demo-token" }
  let(:host) { "https://127.0.0.1:2355" }

  describe "#version" do
    before do
      Excon.stub({
        scheme: "https",
        host: URI(host).hostname,
        path: "/healthz",
        port: URI(host).port,
        headers: {
          "Verification-Token" => token
        }
      },
        {
          body: '{"version": "v3.5.0"}',
          status: 200
        })
    end

    it { expect(client.version).to eq("v3.5.0") }
  end

  describe "#expected_cloning_time" do
    before do
      Excon.stub({
        scheme: "https",
        host: URI(host).hostname,
        path: "/status",
        port: URI(host).port,
        headers: {
          "Verification-Token" => token
        }
      },
        {
          body: '{ "cloning": { "expectedCloningTime": 4.5 } }',
          status: 200
        })
    end

    it { expect(client.expected_cloning_time).to eq(4.5) }
  end
end

# frozen_string_literal: true

RSpec.describe Pgai do
  it "has a version number" do
    expect(Pgai::VERSION).not_to be nil
  end
end

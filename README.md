# Pgai

Simple CLI wrapper around Postgres.ai CLI to drop directly into a PSQL session.

## Installation

Install the gem by executing:

```shell
gem install pgai
```

## Configuration

For an overview of available commands, execute:

```shell
pgai -h
```

Before usage `pgai config` must be executed and at least an environment must be configured with `pgai env add`.

An access token will be required and it can be obtained from: https://console.postgres.ai/gitlab/tokens

Example:

```shell
pgai config --prefix=<gitlab handle>
```

To configure environments:

```shell
pgai env add --alias ci --id ci.lab.internal --port 2345 -n gitlabhq_dblab
```

The environment id and port can be found by clicking on the `Connect` button on a database instance page.

The the id attributes must be configured for SSH port forwarding via the `~/.ssh/config` file. Example:

```shell
Host <bastion>
  User <username>
  IdentityFile ~/.ssh/id_ed25519


Host *.lab.internal
  User <username>
  PreferredAuthentications publickey
  IdentityFile ~/.ssh/id_ed25519
  ProxyCommand ssh <bastion> -W %h:%p
```

## Usage

```shell
pgai connect <env alias>
```

Multiple `connect` commands for an environment will connect to the same clone, it won't start a new one.

`pgai use --only <env alias> -- command` can be used to run commands with access to a clone. Example for connecting a Rails console to the `CI` database:

```shell
pgai use -o ci -v -- bin/rails c -e test
```

### Features

- multiple psql sessions to the same clone
- multiple environments support
- automatic port forward management
- prevents system sleep while psql sessions are active via `caffeinate`

## Upgrading from version 0

- Update and test the ssh config file
- `~/.dblab/` and its contents can be removed
- `~/.config/pgai/config.pstore` should be removed and recreated with `pgai config`
- The environment ID serves as the proxy host and the database name can be configured per environment.
- Environments can share the same remote port value.

## Contributing

Bug reports and pull requests are welcome on GitLab at https://gitlab.com/mbobin/pgai.

# frozen_string_literal: true

module Pgai
  class CloneManager
    def initialize(env)
      @env = env
      @logger = Pgai::Commander.instance.logger
    end

    def connect
      prepare do |cached_clone|
        PsqlManager.new(
          cached_clone,
          logger: logger
        ).run
      end
    end

    def prepare
      find_or_create_clone.with_port_forward do |cached_clone|
        yield cached_clone
      end
    end

    def cleanup
      return unless clone_already_created?

      Resources::Local::Clone.delete(clone_id)
      Resources::Remote::Clone.find(clone_id).tap do |resource|
        resource.delete
        resource.refresh
        logger.info resource.status_message
      end
    end

    def reset
      return unless clone_already_created?

      Resources::Remote::Clone.find(clone_id).tap do |resource|
        resource.reset
        logger.info resource.status_message
      end
    end

    private

    attr_reader :env, :logger

    def clone_id
      env.clone_id
    end

    def find_or_create_clone
      find_cached_clone || create_clone
    end

    def find_cached_clone
      if clone_already_created?
        Resources::Local::Clone.find(clone_id) || raise_unknown_clone
      else
        Resources::Local::Clone.delete(clone_id) && nil
      end
    end

    def create_clone
      CreateCloneService.new(clone_id, env: env, logger: logger).execute
    end

    def raise_unknown_clone
      raise(Pgai::ResourceNotFound, <<~MESSAGE)
        Unknown clone. Remove it and retry.
      MESSAGE
    end

    def clone_already_created?
      !!Resources::Remote::Clone.find(clone_id)
    rescue Pgai::ResourceNotFound
      false
    end
  end
end

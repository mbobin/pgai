# frozen_string_literal: true

require "pathname"
require "pstore"
require "fileutils"

module Pgai
  class Store
    STORE_PATH = "~/.config/pgai/config.pstore"

    def all(record_type)
      store.transaction(true) do
        store[record_type]&.values || {}
      end
    end

    def find(record_type, id)
      store.transaction(true) do
        (store[record_type] || {})[id]
      end
    end

    def delete(record_type, id)
      store.transaction do
        store[record_type] ||= {}
        store[record_type] = store[record_type].except(id)
      end
    end

    def save(record_type, attributes, key: :id)
      store.transaction do
        store[record_type] ||= {}
        store[record_type].merge!(attributes[key] => attributes)
      end
    end

    private

    def store
      @store ||= PStore.new(store_path)
    end

    def store_path
      @store_path ||= Pathname(STORE_PATH).expand_path.tap do |path|
        FileUtils.mkdir_p File.dirname(path)
      end
    end
  end
end

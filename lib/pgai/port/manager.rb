# frozen_string_literal: true

require "concurrent-ruby"

module Pgai
  module Port
    class Manager
      def initialize(logger: Pgai::Commander.instance.logger)
        @logger = logger
        @executor = Concurrent::IndirectImmediateExecutor.new
      end

      def forward(local_port, host, port)
        forwarder = start(local_port, host, port)
        yield
      ensure
        forwarder&.stop
      end

      def start(local_port, host, port)
        fw = Port::Forwarder.new(local_port, host, port, logger: @logger)
        @executor.post(fw, &:start)
        fw
      end
    end
  end
end

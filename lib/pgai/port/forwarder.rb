# frozen_string_literal: true

require "socket"
require "net/ssh"

module Pgai
  module Port
    class Forwarder
      LOCALHOST = "127.0.0.1"

      attr_reader :local_port, :remote_host, :remote_port, :logger

      def initialize(local_port, remote_host, remote_port, logger: Pgai::Commander.instance.logger)
        @local_port = local_port
        @remote_host = remote_host
        @remote_port = remote_port
        @logger = logger
        @child, @parent = Socket.pair(:UNIX, :DGRAM)
      end

      def start
        return if ready?

        debug "starting"
        start_ssh_connection
        wait_until_ready
        debug "ready to accept connections"
      end

      def stop
        return unless @pid

        @parent.write("exit")
        debug "shutting down"
        Process.wait(@pid)
        debug "exit"
        @pid = nil
      end

      private

      def start_ssh_connection
        @pid = Process.fork do
          Signal.trap("INT", "IGNORE")
          @parent.close

          Net::SSH.start(remote_host) do |ssh|
            ssh.forward.local(local_port, LOCALHOST, remote_port)
            ssh.loop(0.1) { keep_running? }
          end
        end
      end

      def keep_running?
        @child.read_nonblock(10).empty?.tap do |value|
          debug "shutdown command received"
        end
      rescue
        true
      end

      def ready?
        !!TCPSocket.new(LOCALHOST, local_port)
      rescue Errno::ECONNREFUSED
        false
      end

      def wait_until_ready
        until ready?
          sleep 0.02
        end
      end

      def debug(message)
        logger.debug "#{logger_inspect} #{message}"
      end

      def logger_inspect
        "[#{self.class}(#{local_port}:#{remote_host}:#{remote_port})]:"
      end
    end
  end
end

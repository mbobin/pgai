# frozen_string_literal: true

require "socket"

module Pgai
  module Port
    class Allocator
      START_PORT = 5000
      FINISH_PORT = 9000
      LOCALHOST = "127.0.0.1"

      def self.pick
        new.pick
      end

      def initialize(start: START_PORT, finish: FINISH_PORT)
        @start = start
        @finish = finish
      end

      def pick
        (start + jitter).upto(finish) do |port|
          return port if available?(port)
        end
      end

      private

      attr_reader :start, :finish

      def jitter
        rand((finish - start) / 2).floor
      end

      def available?(port)
        TCPServer.new(LOCALHOST, port).close
        true
      rescue Errno::EADDRINUSE
        false
      end
    end
  end
end

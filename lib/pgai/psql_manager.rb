# frozen_string_literal: true

module Pgai
  class PsqlManager
    def initialize(cached_clone, logger:)
      @cached_clone = cached_clone
      @logger = logger
    end

    def run
      Signal.trap("INT", "IGNORE")
      logger.info "Data state at: #{cached_clone.data_state_at}"

      psql
    end

    private

    attr_reader :cached_clone, :logger

    def psql
      psql_pid = fork do
        exec("psql #{cached_clone.connection_string}")
      end

      start_caffeinate(psql_pid)
      Process.wait(psql_pid)
    end

    def start_caffeinate(pid)
      return if `which caffeinate`.to_s.empty?

      caffeinate_pid = Process.spawn("caffeinate -is -w #{pid}")
      Process.detach(caffeinate_pid)
    end
  end
end

# frozen_string_literal: true

require "tty-logger"
require "singleton"
require "forwardable"

module Pgai
  class Commander
    include Singleton
    extend Forwardable

    attr_reader :verbosity
    def_delegators :logger, *TTY::Logger::LOG_TYPES.keys

    def self.configure(options)
      instance.tap do |commander|
        if options[:verbose]
          commander.verbosity = :debug
        end

        if options[:quiet]
          commander.verbosity = :error
        end
      end
    end

    def initialize
      self.verbosity = :info
    end

    def store
      @store ||= Pgai::Store.new
    end

    def config
      @config ||= Pgai::Resources::Local::Configuration.default
    end

    def configure
      yield self
    end

    def verbosity=(verbosity)
      @verbosity = verbosity

      TTY::Logger.configure do |config|
        config.level = verbosity
      end
    end

    def logger
      @logger ||= TTY::Logger.new
    end

    def port_manager
      @port_manager ||= Port::Manager.new(logger: logger)
    end

    def check_access_token_presence!
      raise "Access token is not set" unless config&.access_token
    end

    def with_env(env_name, &block)
      check_access_token_presence!

      env = Resources::Local::Environment.find(env_name) do |env|
        env.access_token = config.access_token
        env.clone_prefix = config.clone_prefix
      end

      env.prepare(&block)
    end
  end
end

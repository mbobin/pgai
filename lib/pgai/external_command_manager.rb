# frozen_string_literal: true

module Pgai
  class ExternalCommandManager
    def initialize(env_names, command, logger: nil)
      @env_names = env_names
      @command = command
      @logger = logger || Pgai::Commander.instance.logger
    end

    def run
      Signal.trap("INT", "IGNORE")

      prepare(env_names)
    end

    private

    attr_reader :env_names, :command, :logger

    def prepare(envs, variables = {})
      if (env_name = envs.pop)
        Pgai::Commander.instance.with_env(env_name) do |env|
          Pgai::CloneManager.new(env).prepare do |cached_clone|
            variables["#{env_name.to_s.upcase}_DATABASE_URL"] = cached_clone.database_url
            prepare(envs, variables)
          end
        end
      else
        execute(variables)
      end
    end

    def execute(variables)
      pid = fork do
        exec(variables, *command)
      end
      Process.wait(pid)
    end
  end
end

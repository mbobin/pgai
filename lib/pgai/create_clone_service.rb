# frozen_string_literal: true

require "tty-progressbar"

module Pgai
  class CreateCloneService
    HOSTNAME = "127.0.0.1"
    CLONE_REFRESH_INTERVAL = 0.2
    PROGRESS_BAR_WIDTH = 80

    attr_reader :id, :env, :logger

    def initialize(id, env:, logger:)
      @id = id
      @env = env
      @logger = logger
    end

    def execute
      logger.info("Preparing clone ...")
      logger.debug { "Expected cloning time: #{env.expected_cloning_time}" }
      progress_bar.start

      create_clone_resource

      logger.debug { clone_resource.status_message }
      wait_for_clone_to_be_ready
      logger.debug { clone_resource.status_message }

      Resources::Local::Clone.new(cached_clone_attributes).tap(&:save)
    end

    def create_clone_resource
      @clone_resource = Resources::Remote::Clone.new(id: id).tap do |resource|
        resource.db_object = Resources::Remote::DbObject.new(db_name: env.dbname)
        resource.snapshot = find_snapshot
        resource.save
      end
    end

    def cached_clone_attributes
      {
        id: id,
        port: clone_resource.db_object.port,
        password: clone_resource.db_object.password,
        user: clone_resource.db_object.username,
        host: HOSTNAME,
        remote_host: env.id,
        dbname: env.dbname,
        created_at: clone_resource.created_at,
        data_state_at: clone_resource.snapshot.data_state_at
      }
    end

    def clone_resource
      @clone_resource ||= create_clone_resource
    end

    def find_snapshot
      if env.snapshot
        Resources::Remote::Snapshot.find(env.snapshot)
      else
        Resources::Remote::Snapshot.latest
      end
    end

    def wait_for_clone_to_be_ready
      loop do
        progress_bar.advance(steps)
        clone_resource.refresh
        break if clone_resource.ready?
        sleep(CLONE_REFRESH_INTERVAL)
      end
      progress_bar.finish
    end

    def progress_bar
      @progress_bar ||= TTY::ProgressBar.new("[:bar]", total: PROGRESS_BAR_WIDTH)
    end

    def steps
      @steps ||= (PROGRESS_BAR_WIDTH / env.expected_cloning_time).ceil
    end
  end
end

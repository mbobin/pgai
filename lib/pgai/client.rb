# frozen_string_literal: true

require "excon"

module Pgai
  class Client
    def initialize(token:, host: "http://127.0.0.1:2355")
      @client = Excon.new(host, headers: {"Verification-Token" => token})
    end

    def version
      get("healthz").fetch(:version)
    end

    def expected_cloning_time
      get("status").dig(:cloning, :expected_cloning_time)
    end

    def get(path)
      response = client.get(path: path)
      parse_reponse(response.body)
    end

    def post(path, body = nil)
      response = if body
        client.post(path: path, body: JSON.dump(body))
      else
        client.post(path: path)
      end
      return {} if response.body.empty?

      parse_reponse(response.body)
    end

    def delete(path)
      response = client.delete(path: path)
      return {} if response.body.empty?

      parse_reponse(response.body)
    end

    private

    attr_reader :client

    def parse_reponse(data)
      data = JSON.parse(data)
      transform!(data)
      handle_unauthorized(data)
      data
    end

    def transform_hash!(data)
      data.transform_keys! { |key| key.to_s.gsub(/(?<=[A-Z])(?=[A-Z][a-z])|(?<=[a-z\d])(?=[A-Z])/, "_").downcase.to_sym }
      data.each_value { |value| transform!(value) }
    end

    def transform_array!(data)
      data.each { transform!(_1) }
    end

    def transform!(data)
      if data.is_a?(Hash)
        transform_hash!(data)
      elsif data.is_a?(Array)
        transform_array!(data)
      end
    end

    def handle_unauthorized(data)
      return unless data.is_a?(Hash)
      return unless data[:code] == "UNAUTHORIZED"

      raise UnauthorizedError, data[:message]
    end
  end
end

module Pgai::Cli
  class Env < Base
    desc "add", "Add new environment"
    method_option :alias, aliases: "-a",
      desc: "This will be used internally for the connect command",
      required: true
    method_option :id,
      aliases: "-i",
      desc: "Environment id from postgres.ai, example: lab-ci.db-lab.internal",
      required: true
    method_option :port,
      aliases: "-p",
      desc: "Environment port from postgres.ai",
      required: true
    method_option :dbname,
      aliases: "-n",
      desc: "Specify database name to connect to by default",
      required: true,
      default: "postgres"
    def add
      Pgai::Resources::Local::Environment.new(options).save
    end

    desc "remove", "Remove the specified environment from the config"
    method_option :alias,
      aliases: "-a",
      desc: "The alias used for registering the environment",
      required: true
    def remove
      Pgai::Resources::Local::Environment.delete(options[:alias])
    end

    desc "list", "List all configured environments"
    def list
      data = Pgai::Resources::Local::Environment.all.map do |env|
        env.attributes.slice(:id, :alias, :port, :dbname, :snapshot)
      end

      say JSON.pretty_generate(data)
    end

    desc "dup env", "Duplicate an environment and override the provided attributes"
    method_option :alias, aliases: "-a",
      desc: "This will be used internally for the connect command",
      required: true
    method_option :dbname,
      aliases: "-n",
      desc: "Specify database name to connect to by default"
    method_option :snapshot,
      aliases: "-s",
      desc: "Specify the snapshot id if you want to create a clone from a specific snapshot"
    def dup(env)
      env = Pgai::Resources::Local::Environment.find(env)
      attrs = env.attributes.merge(options.transform_keys(&:to_sym))
      Pgai::Resources::Local::Environment.new(attrs).save
    end
  end
end

module Pgai::Cli
  class Main < Base
    desc "config", "Configure CLI options"
    method_option :prefix, aliases: "-p", desc: "Specify prefix name to be used for clones", required: true
    def config
      token = ask("Access token:", echo: false)

      Pgai::Resources::Local::Configuration
        .new(clone_prefix: options[:prefix], access_token: token)
        .save
    end

    desc "connect database-name", "Create and connect to a thin clone database"
    def connect(name)
      with_env(name) do |env|
        Pgai::CloneManager.new(env).connect
      end
    end

    desc "destroy database-name", "Remove the thin clone and all port forwards"
    def destroy(name)
      with_env(name) do |env|
        Pgai::CloneManager.new(env).cleanup
      end
    end

    desc "reset database-name", "Reset clone's state"
    def reset(name)
      with_env(name) do |env|
        Pgai::CloneManager.new(env).reset
      end
    end

    desc "use", "Execute the given command with DATABASE_URLs"
    method_option :only, aliases: "-o", desc: "Export only this database connection", repeatable: true
    def use(*command)
      envs = options.fetch(:only) { Pgai::Resources::Local::Environment.all.map(&:alias) }

      Pgai::ExternalCommandManager.new(envs, command).run
    end

    desc "env", "Manage environments"
    subcommand "env", Pgai::Cli::Env

    private

    def with_env(name, &block)
      Pgai::Commander.instance.with_env(name, &block)
    end
  end
end

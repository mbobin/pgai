require "thor"

module Pgai::Cli
  class Base < Thor
    def self.exit_on_failure? = true

    check_unknown_options!

    class_option :verbose, type: :boolean, aliases: "-v", desc: "Detailed logging"
    class_option :quiet, type: :boolean, aliases: "-q", desc: "Minimal logging"

    def initialize(*)
      super

      Pgai::Commander.configure(options_with_subcommand_class_options)
    end

    private

    def options_with_subcommand_class_options
      options.merge(@_initializer.last[:class_options] || {})
    end
  end
end

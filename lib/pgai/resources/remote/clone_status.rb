# frozen_string_literal: true

module Pgai
  module Resources
    module Remote
      class CloneStatus < BaseRecord
        attribute :code, :string
        attribute :message, :string
      end
    end
  end
end

# frozen_string_literal: true

module Pgai
  module Resources
    module Remote
      class CloneMetadata < BaseRecord
        attribute :clone_diff_size, :integer
        attribute :logical_size, :integer
        attribute :cloning_time, :decimal
        attribute :max_idle_minutes, :integer
      end
    end
  end
end

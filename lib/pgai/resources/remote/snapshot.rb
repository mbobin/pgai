# frozen_string_literal: true

module Pgai
  module Resources
    module Remote
      class Snapshot < BaseRecord
        attribute :id, :string
        attribute :created_at, :datetime
        attribute :data_state_at, :datetime
        attribute :physical_size, :integer
        attribute :logical_size, :integer
        attribute :pool, :string
        attribute :num_clones, :integer

        class << self
          def all
            client.get(path).map { new(_1) }
          end

          def latest
            all.max_by(&:created_at)
          end

          def path
            "snapshots"
          end

          def find(id)
            record = all.find { |snapshot| snapshot.id.to_s == id.to_s }
            raise Pgai::ResourceNotFound unless record
            record
          end
        end
      end
    end
  end
end

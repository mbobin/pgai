# frozen_string_literal: true

module Pgai
  module Resources
    module Remote
      class Clone < BaseRecord
        attribute :id, :string
        attribute :protected, :boolean, default: false
        attribute :created_at, :datetime
        attribute :delete_at, :datetime

        attr_accessor :db_object
        attr_accessor :snapshot
        attr_accessor :status
        attr_accessor :metadata

        class << self
          def find(id)
            data = client.get(File.join(path, id))
            raise Pgai::ResourceNotFound unless data.key?(:id)

            new(data.slice(:id, :protected)) do |resource|
              resource.refresh_attributes(data)
            end
          end

          def path
            "clone"
          end
        end

        def save
          body = {
            id: id,
            snapshot: {
              id: snapshot.id
            },
            protected: protected,
            db: {
              username: db_object.username,
              password: db_object.password,
              restricted: db_object.restricted
            }
          }

          data = client.post(self.class.path, body)
          refresh_attributes(data)

          self
        end

        def ready?
          status.code == "OK"
        end

        def refresh
          data = client.get(object_path)
          raise Pgai::ResourceNotFound unless data.key?(:id)

          refresh_attributes(data)

          self
        end

        def delete
          data = client.delete(object_path)
          refresh_attributes(data) unless data.empty?

          self
        end

        def reset
          client.post(object_path("reset"))

          self
        end

        def refresh_attributes(data)
          assign_attributes(data.slice(:delete_at, :created_at))
          ensure_metadata.refresh_attributes(data.fetch(:metadata))
          ensure_status.refresh_attributes(data.fetch(:status))
          ensure_db_object.refresh_attributes(data.fetch(:db))
          ensure_snapshot.refresh_attributes(data.fetch(:snapshot))
        end

        def status_message
          status&.message
        end

        private

        def object_path(*fragments)
          File.join(self.class.path, id, *fragments)
        end

        def ensure_metadata
          metadata || self.metadata = CloneMetadata.new
        end

        def ensure_status
          status || self.status = CloneStatus.new
        end

        def ensure_db_object
          db_object || self.db_object = DbObject.new
        end

        def ensure_snapshot
          snapshot || self.snapshot = Snapshot.new
        end
      end
    end
  end
end

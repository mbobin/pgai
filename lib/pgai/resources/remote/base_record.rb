# frozen_string_literal: true

module Pgai
  module Resources
    module Remote
      class BaseRecord
        include Attributes

        class << self
          def client
            @@client ||= nil
          end

          def client=(value)
            @@client = value
          end

          def with_client(new_client)
            previous = client
            self.client = new_client
            yield
          ensure
            self.client = previous
          end
        end

        def client
          self.class.client
        end

        def refresh_attributes(data)
          assign_attributes(data)
        end
      end
    end
  end
end

# frozen_string_literal: true

require "securerandom"

module Pgai
  module Resources
    module Remote
      class DbObject < BaseRecord
        attribute :username, :string, default: -> { SecureRandom.hex(8) }
        attribute :password, :string, default: -> { SecureRandom.hex(16) }
        attribute :restricted, :boolean, default: false
        attribute :host, :string
        attribute :port, :integer
        attribute :db_name, :string
        attribute :conn_str, :string

        def refresh_attributes(data)
          super(data.except(:username, :password))
        end
      end
    end
  end
end

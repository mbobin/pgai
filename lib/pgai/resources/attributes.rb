# frozen_string_literal: true

require "date"

module Pgai
  module Resources
    module Attributes
      def self.included(base)
        base.extend ClassMethods
        attr_reader :attributes
      end

      class Attribute
        FALSE_VALUES = [
          "0", "f", "F", "false", "FALSE",
          "off", "OFF", "NO", "no"
        ].to_set.freeze

        TYPES = {
          string: ->(value) { value.to_s },
          integer: ->(value) { value.to_i },
          decimal: ->(value) { value.to_f },
          boolean: ->(value) { !FALSE_VALUES.include?(value.to_s) },
          datetime: ->(value) { ::Time.at(::Time.new(value.to_s, in: "UTC")) }
        }

        attr_reader :name
        attr_reader :type
        attr_reader :default

        def initialize(name, type, default)
          @name = name
          @type = type
          @default = default

          yield self if block_given?
        end

        def cast(value)
          TYPES.fetch(type).call(value)
        end

        def cast_or_default(user_value)
          value = default
          value = cast(user_value) unless user_value.nil?
          value = value.call if value.respond_to?(:call)
          value
        end
      end

      module ClassMethods
        def inherited(subclass)
          attributes.each do |attr|
            subclass.attribute attr.name, attr.type, default: attr.default
          end
        end

        def attributes
          @attributes ||= []
        end

        def attribute(name, type, default: nil)
          Attribute.new(name, type, default) do |attribute|
            attributes << attribute
            generate_attribute_methods(attribute)
          end
        end

        def generate_attribute_methods(attribute)
          define_method(attribute.name) do
            instance_variable_get(:"@#{attribute.name}")
          end

          define_method("#{attribute.name}=") do |value|
            instance_variable_set(:"@#{attribute.name}", value)
            attributes[attribute.name.to_sym] = value
          end
        end
      end

      def initialize(user_attributes = {})
        @attributes = {}

        self.class.attributes.each do |attribute|
          assign_attribute(attribute, user_attributes[attribute.name])
        end
      end

      def assign_attribute(attribute, user_value)
        value = attribute.cast_or_default(user_value)

        public_send(:"#{attribute.name}=", value)
      end

      def assign_attributes(user_attributes)
        user_attributes.each do |key, value|
          attribute = self.class.attributes.find { |attr| attr.name == key }

          assign_attribute(attribute, value)
        end
      end

      def has_attribute?(name)
        respond_to?(name) && respond_to?(:"#{name}=")
      end
    end
  end
end

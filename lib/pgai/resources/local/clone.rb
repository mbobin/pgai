# frozen_string_literal: true

module Pgai
  module Resources
    module Local
      class Clone < BaseRecord
        attribute :id, :string
        attribute :host, :string
        attribute :remote_host, :string
        attribute :port, :integer
        attribute :password, :string
        attribute :user, :string
        attribute :dbname, :string
        attribute :created_at, :datetime
        attribute :data_state_at, :datetime

        def connection_string
          "'host=#{host} port=#{local_port} user=#{user} dbname=#{dbname} password=#{password}'"
        end

        def database_url
          "postgresql://#{user}:#{password}@#{host}:#{local_port}/#{dbname}"
        end

        def with_port_forward(manager = Pgai::Commander.instance.port_manager)
          manager.forward(local_port, remote_host, port) do
            yield self
          end
        end

        private

        def local_port
          @local_port ||= Port::Allocator.pick
        end
      end
    end
  end
end

# frozen_string_literal: true

require "forwardable"

module Pgai
  module Resources
    module Local
      class BaseRecord
        include Attributes
        extend Forwardable

        def_delegators :klass, :backend, :record_type

        class << self
          def all
            backend.all(record_type).map do |attributes|
              new(attributes)
            end
          end

          def find(id)
            attributes = backend.find(record_type, id)
            return unless attributes

            record = new(attributes)
            yield(record) if block_given?
            record
          end

          def delete(id)
            backend.delete(record_type, id)
            true
          end

          def record_type
            :"#{name.split("::").last.downcase}s"
          end

          def backend
            Pgai::Commander.instance.store
          end
        end

        def save
          backend.save(record_type, attributes, key: store_key)
          true
        end

        def delete
          klass.delete(id)
        end

        def klass
          self.class
        end

        private

        def store_key
          :id
        end
      end
    end
  end
end

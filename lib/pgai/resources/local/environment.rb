# frozen_string_literal: true

module Pgai
  module Resources
    module Local
      class Environment < BaseRecord
        attribute :id, :string
        attribute :alias, :string
        attribute :port, :integer
        attribute :dbname, :string
        attribute :access_token, :string
        attribute :clone_prefix, :string
        attribute :snapshot, :string

        def client
          @client ||= Pgai::Client.new(
            token: access_token,
            host: "http://127.0.0.1:#{local_port}"
          )
        end

        def prepare
          with_port_forward do
            Resources::Remote::BaseRecord.with_client(client) do
              yield(self)
            end
          end
        end

        def with_port_forward(manager = Pgai::Commander.instance.port_manager)
          manager.forward(local_port, id, port) do
            yield self
          end
        end

        def expected_cloning_time
          @expected_cloning_time ||= client.expected_cloning_time
        end

        def local_port
          @local_port ||= Port::Allocator.pick
        end

        def clone_id
          @clone_id ||= "#{clone_prefix}_#{self.alias}"
        end

        private

        def store_key
          :alias
        end
      end
    end
  end
end

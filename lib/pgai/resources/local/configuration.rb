# frozen_string_literal: true

module Pgai
  module Resources
    module Local
      class Configuration < BaseRecord
        attribute :id, :string, default: "config"
        attribute :clone_prefix, :string
        attribute :access_token, :string

        def self.default
          find("config")
        end
      end
    end
  end
end

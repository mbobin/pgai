# frozen_string_literal: true

require "zeitwerk"

loader = Zeitwerk::Loader.for_gem
loader.setup

require "thor"

module Pgai
  class Error < StandardError; end

  class CliError < ::Thor::Error; end

  class ResourceNotFound < Error; end

  class UnauthorizedError < CliError; end
end
